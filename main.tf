resource "helm_release" "postgres-operator" {
  repository = "https://harbor.dcas.dev/chartrepo/library"
  chart = "postgres-operator"
  name = var.name

  version = "1.6.0"

  namespace = var.namespace
  cleanup_on_fail = true
  atomic = true
  max_history = 5

  values = [local.helm_chart_values]

  depends_on = [module.rbac]
}

module "rbac" {
  source = "./rbac"

  namespace = var.namespace
  service-account-name = local.service-account-name
  name = var.name

  enable_crd = var.enable_crd
  enable_nodes = var.enable_nodes
}