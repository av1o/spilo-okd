variable "namespace" {
  type = string
}
variable "name" {
  type = string
}
variable "enable_crd" {
  type = bool
  default = true
}
variable "enable_nodes" {
  type = bool
  default = true
}