locals {
  service-account-name = "${var.name}-operator"
  template_vars = {
    service-account-name = local.service-account-name
    namespace = var.namespace
    name = var.name
  }
  helm_chart_values = templatefile("${path.module}/values-crd.yaml", local.template_vars)
}
