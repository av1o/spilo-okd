# PostgreSQL Operator

This module deploys an instance of the [PostgreSQL Operator](https://github.com/zalando/postgres-operator)
configured for OpenShift 4+

## Changes

* Custom RBAC to reduce need for ClusterRoles
* Default configuration to allow Clusters to run in the `restricted` SCC

## Configuration

| Variable | Description | Default value |
| --- | --- | --- |
| `namespace` | Namespace to deploy the Operator to | |