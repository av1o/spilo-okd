resource "kubernetes_cluster_role" "operator-crd" {
  count = var.enable_crd ? 1 : 0
  metadata {
    name = "${var.name}-operator-crd"
  }
  rule {
    api_groups = ["apiextensions.k8s.io"]
    resources = ["customresourcedefinitions"]
    verbs = ["create", "get", "patch", "update"]
  }
}

resource "kubernetes_cluster_role" "operator-nodes" {
  count = var.enable_nodes ? 1 : 0
  metadata {
    name = "${var.name}-operator-nodes"
  }
  rule {
    api_groups = [""]
    resources = ["nodes"]
    verbs = ["get", "list", "watch"]
  }
}

resource "kubernetes_role" "postgres-pod" {
  metadata {
    name = "${var.name}-pod"
    namespace = var.namespace
  }
  rule {
    api_groups = [""]
    resources = ["configmaps"]
    verbs = ["create", "delete", "deletecollection", "get", "list", "patch", "update", "watch"]
  }
  rule {
    api_groups = [""]
    resources = ["endpoints"]
    verbs = ["get"]
  }
  rule {
    api_groups = [""]
    resources = ["pods"]
    verbs = ["get", "list", "patch", "update", "watch"]
  }
  rule {
    api_groups = [""]
    resources = ["services"]
    verbs = ["create"]
  }
}

resource "kubernetes_role" "operator" {
  metadata {
    name = "${var.name}-operator"
    namespace = var.namespace
  }
  rule {
    api_groups = ["acid.zalan.do"]
    resources = ["postgresqls", "postgresqls/status", "operatorconfigurations"]
    verbs = ["create", "delete", "deletecollection", "get", "list", "patch", "update", "watch"]
  }
  rule {
    api_groups = ["acid.zalan.do"]
    resources = ["postgresteams"]
    verbs = ["get", "list", "watch"]
  }
  rule {
    api_groups = [""]
    resources = ["events"]
    verbs = ["create", "get", "list", "patch", "update", "watch"]
  }
  rule {
    api_groups = [""]
    resources = ["configmaps"]
    verbs = ["create", "delete", "deletecollection", "get", "list", "patch", "update", "watch"]
  }
  rule {
    api_groups = [""]
    resources = ["endpoints"]
    verbs = ["create", "delete", "deletecollection", "get", "list", "patch", "update", "watch"]
  }
  rule {
    api_groups = [""]
    resources = ["secrets"]
    verbs = ["create", "delete", "get", "update"]
  }
  rule {
    api_groups = [""]
    resources = ["persistentvolumeclaims"]
    verbs = ["delete", "get", "list", "patch", "update"]
  }
  rule {
    api_groups = [""]
    resources = ["persistentvolumes"]
    verbs = ["get", "list", "update"]
  }
  rule {
    api_groups = [""]
    resources = ["pods"]
    verbs = ["delete", "get", "list", "patch", "update", "watch"]
  }
  rule {
    api_groups = [""]
    resources = ["pods/exec"]
    verbs = ["create"]
  }
  rule {
    api_groups = [""]
    resources = ["services"]
    verbs = ["create", "delete", "get", "patch", "update"]
  }
  rule {
    api_groups = ["apps"]
    resources = ["statefulsets", "deployments"]
    verbs = ["create", "delete", "get", "list", "patch"]
  }
  rule {
    api_groups = ["batch"]
    resources = ["cronjobs"]
    verbs = ["create", "delete", "get", "list", "patch", "update"]
  }
  rule {
    api_groups = [""]
    resources = ["namespaces"]
    verbs = ["get"]
  }
  rule {
    api_groups = ["policy"]
    resources = ["poddisruptionbudgets"]
    verbs = ["create", "delete", "get"]
  }
  rule {
    api_groups = [""]
    resources = ["serviceaccounts"]
    verbs = ["get", "create"]
  }
  rule {
    api_groups = ["rbac.authorization.k8s.io"]
    resources = ["rolebindings"]
    verbs = ["get", "create"]
  }
}