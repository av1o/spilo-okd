resource "kubernetes_role_binding" "operator" {
  metadata {
    name = kubernetes_role.operator.metadata[0].name
    namespace = var.namespace
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "Role"
    name = kubernetes_role.operator.metadata[0].name
  }
  subject {
    kind = "ServiceAccount"
    name = var.service-account-name
    namespace = var.namespace
  }
}

resource "kubernetes_cluster_role_binding" "operator-crd" {
  count = var.enable_crd ? 1 : 0
  metadata {
    name = kubernetes_cluster_role.operator-crd[0].metadata[0].name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "ClusterRole"
    name = kubernetes_cluster_role.operator-crd[0].metadata[0].name
  }
  subject {
    kind = "ServiceAccount"
    name = var.service-account-name
    namespace = var.namespace
  }
}

resource "kubernetes_cluster_role_binding" "operator-nodes" {
  count = var.enable_nodes ? 1 : 0
  metadata {
    name = kubernetes_cluster_role.operator-nodes[0].metadata[0].name
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "ClusterRole"
    name = kubernetes_cluster_role.operator-nodes[0].metadata[0].name
  }
  subject {
    kind = "ServiceAccount"
    name = var.service-account-name
    namespace = var.namespace
  }
}